package co.selenium.test.trial;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Setup {
	public static WebDriver webDriver = null;
	public static void run(String driver , String url){
	
		System.setProperty(driver, url);


		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();

		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File("crxFile/LeadIQ-Lead-Capture_v5.0.2.crx"));
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		webDriver = new ChromeDriver(capabilities);
	}
}
