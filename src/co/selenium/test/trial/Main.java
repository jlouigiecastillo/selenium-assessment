package co.selenium.test.trial;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Main extends Setup{

	private static final String DRIVER = "webdriver.chrome.driver";
	private static final String URL = "C:\\Users\\john.l.t.castillo\\Downloads\\Selenium\\chromedriver.exe";
	
	public static void main(String[] args) throws InterruptedException {
		Setup.run(DRIVER, URL);
		
		Signup.run();
		//Download.run(driver);

	}

}
