package co.selenium.test.trial;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Download {
	
	public static void run(WebDriver webDriver) throws InterruptedException{
		Thread.sleep(10000);
		webDriver.findElement(By.xpath("/html/body/div/div/div[2]/div[2]")).click();
		Thread.sleep(10000);
		webDriver.findElement(By.id("js-download-hero")).click();
		Thread.sleep(10000);
		webDriver.findElement(By.id("js-accept-install")).click();
	}

}
